#include <stdlib.h>
#include "mem.h"


int mem_alloc_2d(uint8*** pSrc, int width, int height)
{
  int j;

  *pSrc = (uint8**)calloc(sizeof(uint8*), height);
  if ( *pSrc == 0 )
  {
    return -1;
  }

  (*pSrc)[0] = (uint8*)calloc(sizeof(uint8), width * height );

  for (j=1; j< height; j++)
  {
    (*pSrc)[j]  = (*pSrc)[j-1] + width;
  }
  return 0;
}


int mem_alloc_2d_double(double*** pSrc, int width, int height)
{
  int j;

  *pSrc = (double**)calloc(sizeof(double*), height);
  if ( *pSrc == 0 )
  {
    return -1;
  }

  (*pSrc)[0] = (double*)calloc(sizeof(double), width * height );

  for (j=1; j< height; j++)
  {
    (*pSrc)[j]  = (*pSrc)[j-1] + width;
  }
  return 0;
}


int mem_free_2d(uint8*** pSrc)
{
  free((*pSrc)[0]);
  free(*pSrc);
  return 0;
}


int mem_free_2d_double(double*** pSrc)
{
  free((*pSrc)[0]);
  free(*pSrc);
  return 0;
}
