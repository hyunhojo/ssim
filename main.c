//-------------------------------------------------------------------------
// This is an implementation of the algorithm for calculating the
// Structural Similarity (SSIM) index between two raw images.
// Hyunho Jo (hyunhojo@gmail.com)
//
// Reference: https://ece.uwaterloo.ca/~z70wang/research/ssim/
//-------------------------------------------------------------------------
#include <stdio.h>
#include "typedef.h"
#include "mem.h"

#define IMAGE1 "einstein_256x256.raw"
#define IMAGE2 "blur_256x256.raw"
#define WIDTH   256
#define HEIGHT  256
#define C1      6.5025
#define C2      58.5225
#define WINDOW_SIZE 11
#define OFFSET  (WINDOW_SIZE-1)/2

extern double window[11][11];

int main(void)
{
  FILE* fp1 = 0;
  FILE* fp2 = 0;
  uint8** pImg1 = 0;
  uint8** pImg2 = 0;
  double** pUx = 0;
  double** pUy = 0;
  double** pSigmax2 = 0;
  double** pSigmay2 = 0;
  double** pSigmaxy = 0;
  double** result = 0;
  double** pImg1Sq = 0;
  double** pImg2Sq = 0;
  double** pImg1_by_Img2 = 0;
  double ssim_sum = 0; 

  int j, i;
  int y, x; 
  
  mem_alloc_2d(&pImg1, WIDTH, HEIGHT);
  mem_alloc_2d(&pImg2, WIDTH, HEIGHT);

  mem_alloc_2d_double(&pUx, WIDTH-(OFFSET*2), HEIGHT-(OFFSET*2));
  mem_alloc_2d_double(&pUy, WIDTH-(OFFSET*2), HEIGHT-(OFFSET*2));
  mem_alloc_2d_double(&pSigmax2, WIDTH-(OFFSET*2), HEIGHT-(OFFSET*2));
  mem_alloc_2d_double(&pSigmay2, WIDTH-(OFFSET*2), HEIGHT-(OFFSET*2));
  mem_alloc_2d_double(&pSigmaxy, WIDTH-(OFFSET*2), HEIGHT-(OFFSET*2));
  mem_alloc_2d_double(&result, WIDTH-(OFFSET*2), HEIGHT-(OFFSET*2));

  mem_alloc_2d_double(&pImg1Sq, WIDTH, HEIGHT);
  mem_alloc_2d_double(&pImg2Sq, WIDTH, HEIGHT);
  mem_alloc_2d_double(&pImg1_by_Img2, WIDTH, HEIGHT);


  // file read (original raw file)
  fp1 = fopen(IMAGE1, "rb");
  if (fp1 == 0)
  {
    printf("File open errr\n");
    return -1;
  }
  fread( &pImg1[0][0], sizeof(uint8), WIDTH*HEIGHT, fp1);
  fclose(fp1);

  // file read (distorted raw file)
  fp2 = fopen(IMAGE2, "rb");
  if (fp2 == 0)
  {
    printf("File open errr\n");
    return -1;
  }
  fread( &pImg2[0][0], sizeof(uint8), WIDTH*HEIGHT, fp2);
  fclose(fp2);


  // calculate ux and uy for each pixel (convolution)
  for (j=0; j< HEIGHT-(OFFSET*2); j++)
  {
    for (i=0; i< WIDTH-(OFFSET*2); i++)
    {
      double sum = 0;

      for (y=0; y < WINDOW_SIZE; y++)
      {
        for (x=0; x < WINDOW_SIZE; x++)
        {
          sum += (window[y][x] * (double)pImg1[j+y][i+x]); 
        }
      }
      pUx[j][i] = sum;
    }
  }
  
  for (j=0; j< HEIGHT-(OFFSET*2); j++)
  {
    for (i=0; i< WIDTH-(OFFSET*2); i++)
    {
      double sum = 0;

      for (y=0; y < WINDOW_SIZE; y++)
      {
        for (x=0; x < WINDOW_SIZE; x++)
        {
          sum += (window[y][x] * (double)pImg2[j+y][i+x]); 
        }
      }
      pUy[j][i] = sum;
    }
  }

  // Sigmax^2, Signmay^2, and Sigmaxy
  for (j=0; j< HEIGHT; j++)
  {
    for (i=0; i< WIDTH; i++)
    {
      pImg1Sq[j][i] = pImg1[j][i]*pImg1[j][i];
      pImg2Sq[j][i] = pImg2[j][i]*pImg2[j][i];
      pImg1_by_Img2[j][i] = pImg1[j][i]*pImg2[j][i];
    }
  }

  // Sigmax^2
  for (j=0; j< HEIGHT-(OFFSET*2); j++)
  {
    for (i=0; i< WIDTH-(OFFSET*2); i++)
    {
      double sum = 0;

      for (y=0; y < WINDOW_SIZE; y++)
      {
        for (x=0; x < WINDOW_SIZE; x++)
        {
          sum += (window[y][x] * (double)pImg1Sq[j+y][i+x]); 
        }
      }
      pSigmax2[j][i] = sum - (pUx[j][i]*pUx[j][i]);
    }
  }

  // Sigmay^2
  for (j=0; j< HEIGHT-(OFFSET*2); j++)
  {
    for (i=0; i< WIDTH-(OFFSET*2); i++)
    {
      double sum = 0;

      for (y=0; y < WINDOW_SIZE; y++)
      {
        for (x=0; x < WINDOW_SIZE; x++)
        {
          sum += (window[y][x] * (double)pImg2Sq[j+y][i+x]); 
        }
      }
      pSigmay2[j][i] = sum - (pUy[j][i]*pUy[j][i]);
    }
  }

  // Sigmaxy
  for (j=0; j< HEIGHT-(OFFSET*2); j++)
  {
    for (i=0; i< WIDTH-(OFFSET*2); i++)
    {
      double sum = 0;

      for (y=0; y < WINDOW_SIZE; y++)
      {
        for (x=0; x < WINDOW_SIZE; x++)
        {
          sum += (window[y][x] * (double)pImg1_by_Img2[j+y][i+x]); 
        }
      }
      pSigmaxy[j][i] = sum - (pUx[j][i]*pUy[j][i]);
    }
  }


  // SSIM_map
  for (j=0; j< HEIGHT-(OFFSET*2); j++)
  {
    for (i=0; i< WIDTH-(OFFSET*2); i++)
    {
      double ssim = ((2*pUx[j][i]*pUy[j][i]+C1)*(2*pSigmaxy[j][i]+C2))/
                    ((pUx[j][i]*pUx[j][i]+pUy[j][i]*pUy[j][i]+C1)*(pSigmax2[j][i]+pSigmay2[j][i]+C2));
       
      result[j][i] = ssim;
      ssim_sum += ssim;
    }
  }

  printf("SSIM (mean) : %lf\n", ssim_sum / ((HEIGHT-(OFFSET*2))*(WIDTH-(OFFSET*2))) );

  mem_free_2d(&pImg1);
  mem_free_2d(&pImg2);
  mem_free_2d_double(&pUx);
  mem_free_2d_double(&pUy);

  mem_free_2d_double(&pSigmax2);
  mem_free_2d_double(&pSigmay2);
  mem_free_2d_double(&pSigmaxy);
  mem_free_2d_double(&result);
  
  mem_free_2d_double(&pImg1Sq);
  mem_free_2d_double(&pImg2Sq);
  mem_free_2d_double(&pImg1_by_Img2);

  return 0;
}



