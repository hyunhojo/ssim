#ifndef MEM_H
#define MEM_H

#include "typedef.h"

int mem_alloc_2d(uint8*** pSrc, int width, int height);
int mem_alloc_2d_double(double*** pSrc, int width, int height);
int mem_free_2d(uint8*** pSrc);
int mem_free_2d_double(double*** pSrc);


#endif
